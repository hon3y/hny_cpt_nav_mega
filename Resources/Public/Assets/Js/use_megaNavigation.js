/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner :: teufels GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/
var hive_cpt_nav_mega__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        if ( typeof hive_cfg_typoscript__windowLoad == 'undefined') {
        } else {

            if (typeof hive_cfg_typoscript__windowLoad == "boolean" && hive_cfg_typoscript__windowLoad) {

                if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                    console.info('Nav - Mega loaded');
                }

                clearInterval(hive_cpt_nav_mega__interval);

                $("#hive_cpt_nav_mega li").hover(function() {
                    $(this).find('.hive_cpt_nav_mega-wrapper').show();
                }, function(){
                    $(this).find('.hive_cpt_nav_mega-wrapper').hide();
                });

            }

        }

    }

}, 1000);