###############################
## NAVIGATION MEGA CONSTANTS ##
###############################

plugin.tx_hive_cpt_nav_mega {
	settings {
        lib {
            navigation {
                entryLevel = 0
                class {
                    #Level 1: <nav class="">:
                    firstnav = hidden-xs hidden-sm
                    #Level 1: <ul class="">:
                    firstul = clearfix level-1
                    #Level 1: <li class="">:
                    firstli = one
                    #Level 1: <a class="">:
                    firsta = a-lvl-1
                    #Level 2: <div class="meganavigation-wrapper"> (*class is required)
                    div = hidden-xs hidden-sm
                    #Level 2: <nav class="">:
                    secondnav = container meganavigation
                    #Level 2: <ul class="">:
                    secondul = row level-2
                    #Level 2: <li class="">:
                    secondli = two col-sm-4 navSubItem
                    #Level 2: <div class="">:
                    seconddiv = display_table navItemTable
                    #Level 2: <a class="">:
                    seconda = a-lvl-2
                    #Level 2: <span class=""> (Picture):
                    picturespan = display_table-cell navPicture vertical-align_middle
                    #Level 2: <span class=""> (Titile):
                    titlespan = display_table-cell navTitle vertical-align_middle
                }
                role {
                    nav = navigation
                }
                cache {
                    postfix = default
                }
            }
        }
        production {
            includePath {
                public = EXT:hive_cpt_nav_mega/Resources/Public/
                private = EXT:hive_cpt_nav_mega/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_cpt_nav_mega/Resources/Public/
                }

            }
            optional {
                active = 1
            }
        }
    }
}