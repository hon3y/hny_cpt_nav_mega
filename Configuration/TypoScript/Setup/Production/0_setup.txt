###########################
## NAVIGATION MEGA SETUP ##
###########################

page {
    includeJSFooterlibs  {
        hive_cpt_nav_mega__use_js = {$plugin.tx_hive_cpt_nav_mega.settings.production.includePath.public}Assets/Js/use_megaNavigation.min.js
        hive_cpt_nav_mega__use_js.async = 1
    }
}

lib.navigation.mega = COA
lib.navigation.mega {


    5 = LOAD_REGISTER
    5  {
        entryLevel.cObject = TEXT
        entryLevel.cObject {
            field = entryLevel
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.entryLevel}
        }
        classFirstNav.cObject = TEXT
        classFirstNav.cObject {
            field = classFirstNav
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.firstnav}
        }
        roleNav.cObject = TEXT
        roleNav.cObject {
            field = roleNav
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.role.nav}
        }
        classFirstUl.cObject = TEXT
        classFirstUl.cObject {
            field = classFirstUl
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.firstul}
        }
        classFirstLi.cObject = TEXT
        classFirstLi.cObject {
            field = classFirstLi
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.firstli}
        }
        classFirstA.cObject = TEXT
        classFirstA.cObject {
            field = classFirstA
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.firsta}
        }
        classDiv.cObject = TEXT
        classDiv.cObject {
            field = classDiv
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.div}
        }
        classSecondNav.cObject = TEXT
        classSecondNav.cObject {
            field = classSecondNav
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.secondnav}
        }
        classSecondUl.cObject = TEXT
        classSecondUl.cObject {
            field = classSecondUl
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.secondul}
        }
        classSecondLi.cObject = TEXT
        classSecondLi.cObject {
            field = classSecondLi
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.secondli}
        }
        classSecondDiv.cObject = TEXT
        classSecondDiv.cObject {
            field = classSecondDiv
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.seconddiv}
        }
        classSecondA.cObject = TEXT
        classSecondA.cObject {
            field = classA
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.seconda}
        }
        classPictureSpan.cObject = TEXT
        classPictureSpan.cObject {
            field = classPictureSpan
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.picturespan}
        }
        classTitleSpan.cObject = TEXT
        classTitleSpan.cObject {
            field = classTitleSpan
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.class.titlespan}
        }

        cachePostfix.cObject = TEXT
        cachePostfix.cObject {
            field = cachePostfix
            ifEmpty = {$plugin.tx_hive_cpt_nav_mega.settings.lib.navigation.cache.postfix}
        }
    }

    10 = HMENU
    10 {

        noBlur = 1
        entryLevel = TEXT
        entryLevel.data = register:entryLevel
        excludeUidList =

        stdWrap.dataWrap = <nav id="hive_cpt_nav_mega" class="{register:classFirstNav}" role="{register:roleNav}"><ul class="{register:classFirstUl}">|</ul></nav>

        1 = TMENU
        1 {
            expAll = 1

            NO = 1
            NO.stdWrap.field = nav_title // title
            NO.before.dataWrap = <li class="{register:classFirstLi}">|
            NO.wrapItemAndSub = |</li>
            NO.ATagTitle.field = abstract // title
            NO.ATagParams.insertData = 1
            NO.ATagParams = class="{register:classFirstA}" data-id="pid{field:uid}"

            ACT < .NO
            ACT.stdWrap.field = nav_title // title
            ACT.before.dataWrap = <li class="{register:classFirstLi} active">|
            ACT.wrapItemAndSub = |</li>
            ACT.ATagTitle.field = abstract // title
            ACT.ATagParams.insertData = 1
            ACT.ATagParams = class="{register:classFirstA} active" data-id="pid{field:uid}"

            IFSUB < .NO
            IFSUB.stdWrap.field = nav_title // title
            IFSUB.before.dataWrap = <li class="{register:classFirstLi} sub">|
            IFSUB.wrapItemAndSub = |</li>
            IFSUB.ATagParams = class="sub"
            IFSUB.ATagTitle.field = abstract // title
            IFSUB.ATagParams.insertData = 1
            IFSUB.ATagParams = class="{register:classFirstA} sub" data-id="pid{field:uid}"

            ACTIFSUB < .IFSUB
            ACTIFSUB.stdWrap.field = nav_title // title
            ACTIFSUB.before.dataWrap = <li class="{register:classFirstLi} sub active">|
            ACTIFSUB.wrapItemAndSub = |</li>
            IFSUB.stdWrap.field = nav_title // title
            IFSUB.ATagTitle.field = abstract // title
            IFSUB.ATagParams.insertData = 1
            IFSUB.ATagParams = class="{register:classFirstA} sub active" data-id="pid{field:uid}"
        }

        2 = TMENU
        2 {
            expAll = 1
            stdWrap.dataWrap = <div class="hive_cpt_nav_mega-wrapper {register:classDiv}" style="display: none;"><nav id="meganavigation" class="{register:classSecondNav}" role="navigation"><ul class="{register:classSecondUl}">|</ul></nav></div>

            NO = 1
            NO {
                allWrap.insertData = 1
                allWrap = |*| <li class="{register:classSecondLi}"><div class="{register:classSecondDiv}"> | </div></li> || <li class="{register:classSecondLi}"><div class="{register:classSecondDiv}"> | </div></li> || <li class="{register:classSecondLi}"><div class="{register:classSecondDiv}"> | </div></li><li class="clearfix"></li> || <li class="{register:classSecondLi}"><div class="{register:classSecondDiv}"> | </div></li> || <li class="{register:classSecondLi}"><div class="{register:classSecondDiv}"> | </div></li> || <li class="{register:classSecondLi}"><div class="{register:classSecondDiv}"> | </div></li> |*|

                ATagTitle.field = abstract // title
                ATagParams.insertData = 1
                ATagParams = class="{register:classSecondA}" data-id="pid{field:uid}"

                stdWrap.cObject = COA
                stdWrap.cObject {

                    5 = FILES
                    5 {
                        references {
                            table = pages
                            uid.data = field = uid
                            fieldName = media
                        }

                        # drittes Bild auslesen
                        begin = 0
                        # ein Bild auslesen
                        maxItems = 1

                        renderObj = IMAGE
                        renderObj {
                            file.import.data = file:current:uid
                            file.treatIdAsReference = 1
                            titleText.data = file:current:title
                            altText.data = file:current:alternative
                            stdWrap.dataWrap = <span class="{register:classPictureSpan}">|</span>
                        }
                    }

                    40 = TEXT
                    40.field = nav_title // title
                    40.stdWrap.dataWrap = <span class="{register:classTitleSpan}">|</span>
                }
            }

            ACT < .NO
            ACT.ATagTitle.field = abstract // title
            ACT.ATagParams.insertData = 1
            ACT.ATagParams = class="{register:classSecondA} active" data-id="pid{field:uid}"

            IFSUB < .NO
            IFSUB.ATagTitle.field = abstract // title
            IFSUB.ATagParams.insertData = 1
            IFSUB.ATagParams = class="{register:classSecondA} sub" data-id="pid{field:uid}"

            ACTIFSUB < .IFSUB
            ACTIFSUB.ATagTitle.field = abstract // title
            ACTIFSUB.ATagParams.insertData = 1
            ACTIFSUB.ATagParams = class="{register:classSecondA} sub active" data-id="pid{field:uid}"

        }
    }
}

# Caching for Menu
[globalVar = LIT:1 = {$plugin.tx_hive_cpt_nav_mega.settings.production.optional.active}]
lib.navigation.mega {
    stdWrap {
        cache {
            /**
             * Hier wird die aktuelle Sprache benutzt, um einen Inhalt pro Sprache
             * für alle Seiten zu cachen.
             */
            key = tx_hive_cpt_nav_mega_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            key.insertData = 1

            // Mit den selbstgewählten Tags kann der Cache später gezielt geleert werden.
            tags = tx_hive_cpt_nav_mega_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            tags.insertData = 1

            /**
             * Wenn eine neue Seite generiert wird, ist das Kontaktmenü max. eine Stunde alt.
             * Wenn die Seite alle 24 Stunden neu generiert wird, könnte das Kontaktmenü
             * also auch über einen Tag alt sein!
             */
            # 3600 x 24 x 30
            lifetime = 2592000
        }
    }

    4 = TEXT
    4 {
        data = date: dmyHis
        wrap = <!--CACHED_|-->
    }
}
[global]